import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';

import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ProductModule } from './product/product.module';
import { SupabaseModule } from './supabase/supabase.module';

@Module({
    imports: [
        ConfigModule.forRoot({
            isGlobal: true,
        }),
        ProductModule,
        SupabaseModule
    ],
    controllers: [AppController],
    providers: [AppService],
})
export class AppModule {
}
