export default class CustomConsole {
    static Reset = "\x1b[0m";
    static Bright = "\x1b[1m";
    static FgBlack = "\x1b[30m";
    static FgRed = "\x1b[31m";
    static FgGreen = "\x1b[32m";
    static FgYellow = "\x1b[33m";
    static FgBlue = "\x1b[34m";
    static FgMagenta = "\x1b[35m";
    static FgCyan = "\x1b[36m";
    static FgWhite = "\x1b[37m";

    static log(message: string, color: string = CustomConsole.Reset) {
        console.log(`${color}${message}${CustomConsole.Reset}`);
    }

    static error(message: string, color: string = CustomConsole.Reset) {
        console.error(`${color}${message}${CustomConsole.Reset}`);
    }
}