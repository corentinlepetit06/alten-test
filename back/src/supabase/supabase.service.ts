import { Inject, Injectable, Scope } from '@nestjs/common';
import { Request } from 'express';
import { REQUEST } from '@nestjs/core';
import { ConfigService } from '@nestjs/config';

import { createClient } from '@supabase/supabase-js';

import CustomConsole from "../utils/custom.log";
import {Database} from "./supabase";

@Injectable({ scope: Scope.REQUEST })
export class SupabaseService {
    private clientInstance;

    constructor(
        @Inject(REQUEST) private readonly request: Request,
        private readonly configService: ConfigService,
    ) {}

    async getClient() {
        CustomConsole.log(
            `Getting Supbabase Client`,
            CustomConsole.FgGreen
        );

        if (this.clientInstance) {
            CustomConsole.log(
                `Supbabase Client already exist`,
                CustomConsole.FgYellow
            );
            return this.clientInstance;
        }

        CustomConsole.log(
            `Creating new Supbabase Client`,
            CustomConsole.FgBlue
        );
        this.clientInstance = createClient<Database>(
            this.configService.get('SUPABASE_API_URL'),
            this.configService.get('SUPABASE_KEY'),
        );
        return this.clientInstance;
    }
}