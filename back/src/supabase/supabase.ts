export type Json =
  | string
  | number
  | boolean
  | null
  | { [key: string]: Json | undefined }
  | Json[]

export interface Database {
  public: {
    Tables: {
      product: {
        Row: {
          category: string
          code: string
          description: string
          id: number
          image: string | null
          inventory_status: string
          name: string
          price: number
          quantity: number
          rating: number | null
        }
        Insert: {
          category: string
          code: string
          description: string
          id?: number
          image?: string | null
          inventory_status: string
          name: string
          price: number
          quantity: number
          rating?: number | null
        }
        Update: {
          category?: string
          code?: string
          description?: string
          id?: number
          image?: string | null
          inventory_status?: string
          name?: string
          price?: number
          quantity?: number
          rating?: number | null
        }
        Relationships: []
      }
    }
    Views: {
      [_ in never]: never
    }
    Functions: {
      [_ in never]: never
    }
    Enums: {
      [_ in never]: never
    }
    CompositeTypes: {
      [_ in never]: never
    }
  }
}
