import {HttpException, HttpStatus, Injectable} from '@nestjs/common';
import {ProductDto} from "./dto/product.dto";
import {SupabaseService} from "../supabase/supabase.service";
import {response} from "express";

@Injectable()
export class ProductService {
    constructor(private readonly supabaseService: SupabaseService) {
    }

    private async getSupabaseClient() {
        try {
            return await this.supabaseService.getClient();
        } catch (error) {
            throw new Error('Cannot connect to Supabase');
        }
    }

    public async create(request: ProductDto): Promise<ProductDto> {
        const supabaseClient = await this.getSupabaseClient();
        if (await this.checkIfProductExist(request.id)) {
            throw new HttpException(`Product already exist : ${request.id}`, HttpStatus.CONFLICT);
        }
        const {data, error} = await supabaseClient
            .from('product')
            .insert([{
                id: request.id,
                code: request.code,
                name: request.name,
                description: request.description,
                price: request.price,
                quantity: request.quantity,
                inventory_status: request.inventoryStatus,
                category: request.category,
                image: request.image ? request.image : null,
                rating: request.rating ? request.rating : null
            }])
            .select();
        if (error) {
            throw new HttpException(`Failed to create product : ${error.message}`, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return data[0];
    }

    public async findAll(): Promise<ProductDto[]> {
        const supabaseClient = await this.getSupabaseClient();
        let { data: product, error } = await supabaseClient
            .from('product')
            .select('*');
        if (error) {
            throw new HttpException(`Failed to find all product : ${error.message}`, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return product;
    }

    public async findOne(id: number): Promise<ProductDto> {
        const supabaseClient = await this.getSupabaseClient();
        let { data: product, error } = await supabaseClient
            .from('product')
            .select('*')
            .eq('id', id);
        if (error) {
            throw new HttpException(`Failed to find one product : ${error.message}`, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return product[0];
    }

    public async update(id: number, request: ProductDto): Promise<ProductDto> {
        const supabaseClient = await this.getSupabaseClient();
        if (!await this.checkIfProductExist(id)) {
            throw new HttpException(`Product does not exist : ${id}`, HttpStatus.BAD_REQUEST);
        }
        const { data, error } = await supabaseClient
            .from('product')
            .upsert({
                id: request.id,
                code: request.code,
                name: request.name,
                description: request.description,
                price: request.price,
                quantity: request.quantity,
                inventory_status: request.inventoryStatus,
                category: request.category,
                image: request.image ? request.image : null,
                rating: request.rating ? request.rating : null
            })
            .select();
        if (error) {
            throw new HttpException(`Failed to create product : ${error.message}`, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return data[0];
    }

    public async delete(id: number): Promise<HttpException> {
        const supabaseClient = await this.getSupabaseClient();
        if (!await this.checkIfProductExist(id)) {
            throw new HttpException(`Product does not exist : ${id}`, HttpStatus.BAD_REQUEST);
        }
        const { error } = await supabaseClient
            .from('product')
            .delete()
            .eq('id', id);
        if (error) {
            throw new HttpException(`Failed to delete product : ${error.message}`, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new HttpException(`Product with id ${id} deleted succesfully`, HttpStatus.OK)
    }

    private async checkIfProductExist(id: number): Promise<Boolean> {
        const product: ProductDto = await this.findOne(id);
        return product !== undefined;
    }
}