import { Module } from '@nestjs/common';
import { ProductService } from './product.service';
import { ProductController } from './product.controller';
import {SupabaseModule} from "../supabase/supabase.module";

@Module({
  imports: [SupabaseModule],
  providers: [ProductService],
  controllers: [ProductController],
})
export class ProductModule {}
