import {Controller, Post, Body, Param, Get, Delete, Patch, HttpException} from '@nestjs/common';
import { ApiTags } from "@nestjs/swagger";

import { ProductService } from "./product.service";
import { ProductDto } from "./dto/product.dto";

@ApiTags('Products')
@Controller('products')
export class ProductController {
    constructor(private readonly productService: ProductService) {}

    @Post()
    public create(@Body() request: ProductDto): Promise<ProductDto> {
        return this.productService.create(request);
    }

    @Get()
    public findAll(): Promise<ProductDto[]> {
        return this.productService.findAll();
    }

    @Get(':id')
    public findOne(@Param('id') id: string): Promise<ProductDto> {
        return this.productService.findOne(+id);
    }


    @Patch(':id')
    public update(@Param('id') id: string, @Body() request: ProductDto): Promise<ProductDto> {
        return this.productService.update(+id, request);
    }


    @Delete(':id')
    public delete(@Param('id') id: string): Promise<HttpException> {
        return this.productService.delete(+id);
    }
}
